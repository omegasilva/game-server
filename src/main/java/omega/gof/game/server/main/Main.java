package omega.gof.game.server.main;

import omega.gof.game.server.core.Engine;
import omega.gof.game.server.core.GameActionType;
import omega.gof.game.server.model.environments.EnvironmentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        LOGGER.info("Initializing server");

        Engine engine = new Engine();
        engine.initialize();

        String session = engine.createNewGame(EnvironmentType.EUROPE_DARK_AGE);

        engine.handleGameRequest(session, GameActionType.ATTACK);

        engine.handleGameRequest(session, GameActionType.CHANGE_STRATEGY_BRUTE_FORCE);
        engine.handleGameRequest(session, GameActionType.ATTACK);
    }
}
