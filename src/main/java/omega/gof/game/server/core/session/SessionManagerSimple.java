package omega.gof.game.server.core.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SessionManagerSimple {

    private Map<UUID, Long> sessionStore;
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionManagerSimple.class);

    private static final SessionManagerSimple INSTANCE = new SessionManagerSimple();

    private SessionManagerSimple() {
        LOGGER.info("Initializing SessionManagerSimple");
        sessionStore = new HashMap<>();
    }

    public static SessionManagerSimple getInstance() {
        return INSTANCE;
    }

    public String initSession() {
        UUID uuid = UUID.randomUUID();
        this.sessionStore.put(uuid, System.currentTimeMillis());

        LOGGER.info("Generated session : " + uuid.toString());
        return uuid.toString();
    }
}
