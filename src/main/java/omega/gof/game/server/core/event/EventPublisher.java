package omega.gof.game.server.core.event;

import java.util.ArrayList;
import java.util.List;

public class EventPublisher {

    private List<EventSubscriber> subscribers;

    public EventPublisher() {
        subscribers = new ArrayList<>();
    }

    public void subscribe(EventSubscriber eventSubscriberParam) {
        this.subscribers.add(eventSubscriberParam);
    }

    public void unsubscribe(EventSubscriber eventSubscriberParam) {
        this.subscribers.remove(eventSubscriberParam);
    }

    public void notifySubscribers(EventType eventType) {
        for (EventSubscriber eventSubscriber : subscribers) {
            eventSubscriber.update(eventType);
        }
    }

}
