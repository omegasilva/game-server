package omega.gof.game.server.core.strategy.attack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BruteForce implements AttackStrategy {

    private static final Logger LOGGER = LoggerFactory.getLogger(BruteForce.class);

    private String name = "BruteForce";

    @Override
    public List<String> findEnemiesToAttack(String currentPosition) {
        LOGGER.info("Finding closest enemies to attack with position : " + currentPosition);
        return new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }
}
