package omega.gof.game.server.core.strategy.attack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class DivideAndConquer implements AttackStrategy{

    private static final Logger LOGGER = LoggerFactory.getLogger(DivideAndConquer.class);

    private String name = "DivideAndConquer";

    @Override
    public List<String> findEnemiesToAttack(String currentPosition) {
        LOGGER.info("Finding enemies coordinating with other soldiers with position : " + currentPosition);
        return new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }
}
