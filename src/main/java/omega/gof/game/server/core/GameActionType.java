package omega.gof.game.server.core;

public enum GameActionType {
    COLLECT_RESOURCES,
    DEFEND,
    ATTACK,
    CHANGE_STRATEGY_BRUTE_FORCE
}
