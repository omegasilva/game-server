package omega.gof.game.server.core.session;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SessionManagerThreadSafe {

    private Map<UUID, Long> sessionStore;
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionManagerSimple.class);

    private static volatile SessionManagerThreadSafe INSTANCE;

    private SessionManagerThreadSafe() {
        LOGGER.info("Initializing SessionManagerSimple");
        sessionStore = new HashMap<>();
    }

    public static SessionManagerThreadSafe getInstance() {
        if (INSTANCE == null) {
            synchronized (SessionManagerThreadSafe.class) {
                if (INSTANCE == null) {
                    INSTANCE = new SessionManagerThreadSafe();
                }
            }
        }

        return INSTANCE;
    }

    public String initSession() {
        UUID uuid = UUID.randomUUID();
        this.sessionStore.put(uuid, System.currentTimeMillis());

        LOGGER.info("Generated session : " + uuid.toString());
        return uuid.toString();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException("This is a Singleton");
    }
}
