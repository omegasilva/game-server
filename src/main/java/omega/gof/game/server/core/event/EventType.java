package omega.gof.game.server.core.event;

public enum EventType {
    COLLECT_WOOD,
    HARVEST,
    ATTACK_ENEMY,
    INITIATE_RECON_MISSIONS,
    DEFEND_GROUND,
    RETURN_TO_BASE,
    RETREAT
}
