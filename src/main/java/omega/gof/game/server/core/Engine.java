package omega.gof.game.server.core;

import omega.gof.game.server.model.Game;
import omega.gof.game.server.model.environments.Environment;
import omega.gof.game.server.model.environments.EnvironmentRegistry;
import omega.gof.game.server.model.environments.EnvironmentType;
import omega.gof.game.server.core.session.SessionManagerSimple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class Engine {

    private Map<String, Game> activeGameMap;

    private static final Logger LOGGER = LoggerFactory.getLogger(Engine.class);

    public Engine() {
        this.activeGameMap = new HashMap<>();
    }

    public void initialize() {
        LOGGER.info("Initializing game engine");
        // Initialize Environment Registry
        EnvironmentRegistry.initialize();
    }

    public String createNewGame(EnvironmentType environmentType) {
        // Get an instance of session manager
        SessionManagerSimple sessionManager = SessionManagerSimple.getInstance();

        // Initialize a session
        String session = sessionManager.initSession();

        // Get a pre-set environment
        Environment gameEnvironment = EnvironmentRegistry.getEnvironment(environmentType);

        Game game = new Game();
        game.setSessionId(session);
        game.setEnvironment(gameEnvironment);
        game.setEnvironmentType(environmentType);
        game.initialize();

        this.activeGameMap.put(session, game);

        return session;
    }

    public void handleGameRequest(String session, GameActionType gameActionType) {
        Game currentGame = this.activeGameMap.get(session);
        currentGame.handleGameAction(gameActionType);
    }


}
