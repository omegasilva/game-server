package omega.gof.game.server.core.strategy.attack;

import java.util.List;

public interface AttackStrategy {

    public List<String> findEnemiesToAttack(String currentPosition);

    public String getName();
}
