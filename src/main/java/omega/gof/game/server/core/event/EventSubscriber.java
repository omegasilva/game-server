package omega.gof.game.server.core.event;

public interface EventSubscriber {

    public void update(EventType eventType);
}
