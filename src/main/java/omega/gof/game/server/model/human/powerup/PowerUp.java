package omega.gof.game.server.model.human.powerup;

public interface PowerUp {

    public int getPowerUpFactor();
}
