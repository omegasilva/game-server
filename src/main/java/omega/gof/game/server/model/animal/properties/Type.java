package omega.gof.game.server.model.animal.properties;

public enum Type {
    BIRD,
    FOUR_LEGGED
}
