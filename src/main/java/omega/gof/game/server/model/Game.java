package omega.gof.game.server.model;

import omega.gof.game.server.core.GameActionType;
import omega.gof.game.server.core.event.EventPublisher;
import omega.gof.game.server.core.event.EventType;
import omega.gof.game.server.core.strategy.attack.BruteForce;
import omega.gof.game.server.model.environments.Environment;
import omega.gof.game.server.model.environments.EnvironmentType;

public class Game {

    private Environment environment;
    private EnvironmentType environmentType;

    private EventPublisher eventPublisher;
    private String sessionId;

    public void initialize() {
        this.eventPublisher = new EventPublisher();
        environment.subscribeToGameEventPublisher(eventPublisher);
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public EventPublisher getEventPublisher() {
        return eventPublisher;
    }

    public void setEventPublisher(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public EnvironmentType getEnvironmentType() {
        return environmentType;
    }

    public void setEnvironmentType(EnvironmentType environmentType) {
        this.environmentType = environmentType;
    }

    public void handleGameAction(GameActionType gameActionType) {
        if (gameActionType.equals(GameActionType.COLLECT_RESOURCES)) {
            eventPublisher.notifySubscribers(EventType.COLLECT_WOOD);
            eventPublisher.notifySubscribers(EventType.HARVEST);
        } else if (gameActionType.equals(GameActionType.ATTACK)) {
            eventPublisher.notifySubscribers(EventType.ATTACK_ENEMY);
            eventPublisher.notifySubscribers(EventType.INITIATE_RECON_MISSIONS);
        } else if (gameActionType.equals(GameActionType.CHANGE_STRATEGY_BRUTE_FORCE)) {
            this.environment.setAttackStrategy(new BruteForce());
        }
    }
}
