package omega.gof.game.server.model.human.powerup;

import omega.gof.game.server.model.human.Human;
import omega.gof.game.server.model.human.NonPlayingHuman;

public abstract class ExperiencePowerUp implements PowerUp {

    private PowerUp powerUp;

    public ExperiencePowerUp(PowerUp powerUp) {
        this.powerUp = powerUp;
    }

    @Override
    public int getPowerUpFactor() {
        return this.powerUp.getPowerUpFactor();
    }
}
