package omega.gof.game.server.model.environments;

public class StoneAgeEnvironment extends Environment{

    @Override
    protected void initializeSoldersSword() {

    }

    @Override
    protected void initializeSoldersGun() {

    }

    @Override
    protected void initializeSoldersStrategy() {

    }

    @Override
    protected void initializeCivilianFemaleFarmers() {

    }

    @Override
    protected void initializeCivilianMaleFarmers() {

    }

    @Override
    public void initializeEnvironment() {

    }
}
