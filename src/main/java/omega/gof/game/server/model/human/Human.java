package omega.gof.game.server.model.human;

import omega.gof.game.server.core.event.EventSubscriber;
import omega.gof.game.server.core.event.EventType;
import omega.gof.game.server.model.environments.Environment;
import omega.gof.game.server.model.human.powerup.PowerUp;
import omega.gof.game.server.model.human.properties.*;

public abstract class Human implements EventSubscriber {
    protected Armor armor;
    protected Intelligence intelligence;
    protected Loyalty loyalty;
    protected MovementPattern movementPattern;
    protected Skill skill;
    protected Skin skin;
    protected Stamina stamina;
    protected Type type;

    protected PowerUp powerUp;
    protected long birthTimeStamp;

    protected Environment environment;

    public abstract void initialize();

    public abstract void changeBehavior(EventType eventType);

    public PowerUp getPowerUp() {
        return powerUp;
    }

    public void setPowerUp(PowerUp powerUp) {
        this.powerUp = powerUp;
    }

    public long getBirthTimeStamp() {
        return birthTimeStamp;
    }

    public Intelligence getIntelligence() {
        return intelligence;
    }

    public Loyalty getLoyalty() {
        return loyalty;
    }

    public Type getType() {return type;}

    public void setLoyalty(Loyalty loyaltyParam) {
        this.loyalty = loyaltyParam;
    }

    public void setIntelligence(Intelligence intelligence) {
        this.intelligence = intelligence;
    }
}
