package omega.gof.game.server.model.animal;

import omega.gof.game.server.core.event.EventSubscriber;
import omega.gof.game.server.model.animal.properties.Type;

public abstract class Animal implements EventSubscriber {

    protected Type animalType;

    public abstract void initialize();
}
