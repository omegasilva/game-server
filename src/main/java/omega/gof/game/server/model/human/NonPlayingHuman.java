package omega.gof.game.server.model.human;

import omega.gof.game.server.core.event.EventType;
import omega.gof.game.server.core.strategy.attack.AttackStrategy;
import omega.gof.game.server.model.environments.Environment;
import omega.gof.game.server.model.human.powerup.BasicPowerUp;
import omega.gof.game.server.model.human.powerup.PowerUp;
import omega.gof.game.server.model.human.properties.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class NonPlayingHuman extends Human {

    private final Logger SUPER_LOGGER = LoggerFactory.getLogger(NonPlayingHuman.class);

    public NonPlayingHuman(Armor armor, Intelligence intelligence, Loyalty loyalty, MovementPattern movementPattern,
                           Skill skill, Skin skin, Stamina stamina, Type type, Environment environment) {
        this.armor = armor;
        this.intelligence = intelligence;
        this.loyalty = loyalty;
        this.movementPattern = movementPattern;
        this.skill = skill;
        this.skin = skin;
        this.stamina = stamina;
        this.type = type;
        this.environment = environment;
        this.powerUp = new BasicPowerUp();
        this.birthTimeStamp = System.currentTimeMillis();
    }

    public NonPlayingHuman() {
        // Default constructor
    }

    @Override
    public void initialize() {
        SUPER_LOGGER.info("Initializing NonPlayingHuman");
    }

    @Override
    public void changeBehavior(EventType eventType) {
        if (eventType.equals(EventType.ATTACK_ENEMY)) {
            AttackStrategy attackStrategy = this.environment.getAttackStrategy();
            List<String> enemiesToAttack = this.environment.getAttackStrategy().findEnemiesToAttack("current");
            SUPER_LOGGER.info("Got enemies to track with Attack Strategy : " + attackStrategy.getName());
        }
    }

    @Override
    public void update(EventType eventType) {
        SUPER_LOGGER.info("Event received : " + eventType.name());
        changeBehavior(eventType);
    }



    public static class NonPlayingHumanBuilder {
        private final Logger LOGGER = LoggerFactory.getLogger(NonPlayingHumanBuilder.class);

        private Armor armor = Armor.LOW;
        private Intelligence intelligence = Intelligence.LOW;
        private Loyalty loyalty = Loyalty.LOW;
        private MovementPattern movementPattern = MovementPattern.RANDOM;
        private Skill skill = Skill.FARMING;
        private Skin skin = Skin.VILLAGER;
        private Stamina stamina = Stamina.LOW;
        private Type type = Type.CIVILIAN_FEMALE;
        private Environment environment;
        private PowerUp powerUp;
        private long birthTimeStamp;


        public NonPlayingHumanBuilder(Environment environment) {
            this.environment = environment;
            this.powerUp = new BasicPowerUp();
            this.birthTimeStamp = System.currentTimeMillis();
            LOGGER.info("Initializing NPCBuilder");
        }

        public NonPlayingHumanBuilder withArmor(Armor armorParam) {
            this.armor = armorParam;
            return this;
        }

        public NonPlayingHumanBuilder withIntelligence(Intelligence intelligenceParam) {
            this.intelligence = intelligenceParam;
            return this;
        }

        public NonPlayingHumanBuilder withLoyality(Loyalty loyaltyPram) {
            this.loyalty = loyaltyPram;
            return this;
        }

        public NonPlayingHumanBuilder withMovementPattern(MovementPattern movementPatternParam) {
            this.movementPattern = movementPatternParam;
            return this;
        }

        public NonPlayingHumanBuilder withSkill(Skill skillParam) {
            this.skill = skillParam;
            return this;
        }

        public NonPlayingHumanBuilder withSkin(Skin skinParam) {
            this.skin = skinParam;
            return this;
        }

        public NonPlayingHumanBuilder withStamina(Stamina staminaParam) {
            this.stamina = staminaParam;
            return this;
        }

        public NonPlayingHumanBuilder withType(Type typeParam) {
            this.type = typeParam;
            return this;
        }

        public NonPlayingHuman build() {
            NonPlayingHuman nph = new NonPlayingHuman();
            nph.armor = this.armor;
            nph.intelligence = this.intelligence;
            nph.loyalty = this.loyalty;
            nph.movementPattern = this.movementPattern;
            nph.skill = this.skill;
            nph.skin = this.skin;
            nph.type = this.type;
            nph.environment = this.environment;
            nph.powerUp = this.powerUp;
            nph.birthTimeStamp = this.birthTimeStamp;

            return nph;
        }
    }

}
