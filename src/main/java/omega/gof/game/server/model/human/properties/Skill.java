package omega.gof.game.server.model.human.properties;

public enum Skill {
    DEFAULT,
    FARMING,
    WOODWORK,
    BATTLE_SWORD,
    BATTLE_GUN,
    BATTLE_STRATEGY
}
