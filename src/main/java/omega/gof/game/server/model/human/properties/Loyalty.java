package omega.gof.game.server.model.human.properties;

public enum Loyalty implements Level {
    HIGH,
    MEDIUM,
    LOW
}
