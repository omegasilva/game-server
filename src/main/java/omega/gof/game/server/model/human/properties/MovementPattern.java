package omega.gof.game.server.model.human.properties;

public enum MovementPattern {
    RANDOM,
    FUNCTION
}
