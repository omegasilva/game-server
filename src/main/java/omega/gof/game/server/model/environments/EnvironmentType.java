package omega.gof.game.server.model.environments;

public enum EnvironmentType {
    STONE_AGE,
    EUROPE_DARK_AGE,
    FIRST_WORLD_WAR_EUROPE,
    SECOND_WORLD_WAR_D_DAY,
    NUCLEAR_AGE
}
