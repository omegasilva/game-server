package omega.gof.game.server.model.human.properties;

public enum Stamina {
    HIGH,
    MEDIUM,
    LOW
}
