package omega.gof.game.server.model.animal;

import omega.gof.game.server.core.event.EventType;
import omega.gof.game.server.model.human.InteractiveHuman;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InteractiveAnimal extends Animal {

    private final Logger LOGGER = LoggerFactory.getLogger(InteractiveAnimal.class);

    @Override
    public void initialize() {
        LOGGER.info("Initialize Interactive Animal");
    }

    @Override
    public void update(EventType eventType) {
        LOGGER.info("Event received : " + eventType.name());
    }
}
