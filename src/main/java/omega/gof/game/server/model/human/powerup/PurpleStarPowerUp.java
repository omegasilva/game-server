package omega.gof.game.server.model.human.powerup;

import omega.gof.game.server.model.human.NonPlayingHuman;

public class PurpleStarPowerUp extends ExperiencePowerUp {

    final int POWER_UP_ADVANTAGE = 4;

    public PurpleStarPowerUp(PowerUp powerUp) {
        super(powerUp);
    }

    @Override
    public int getPowerUpFactor() {
        return super.getPowerUpFactor() + POWER_UP_ADVANTAGE;
    }
}
