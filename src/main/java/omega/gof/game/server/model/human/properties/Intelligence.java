package omega.gof.game.server.model.human.properties;

public enum Intelligence {
    HIGH,
    MEDIUM,
    LOW
}
