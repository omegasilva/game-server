package omega.gof.game.server.model.human;

import omega.gof.game.server.core.event.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InteractiveHuman extends Human {

    private final Logger LOGGER = LoggerFactory.getLogger(InteractiveHuman.class);

    @Override
    public void initialize() {
        LOGGER.info("Initialized Interactive Human");
    }

    @Override
    public void update(EventType eventType) {
        LOGGER.info("Event received : " + eventType.name());
    }

    @Override
    public void changeBehavior(EventType eventType) {

    }
}
