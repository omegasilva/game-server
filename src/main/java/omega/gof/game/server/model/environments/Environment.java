package omega.gof.game.server.model.environments;

import omega.gof.game.server.ai.AIFacade;
import omega.gof.game.server.ai.LoyaltyRegressionModel;
import omega.gof.game.server.ai.LoyaltyRegressionModelAdapter;
import omega.gof.game.server.ai.MLModel;
import omega.gof.game.server.core.event.EventPublisher;
import omega.gof.game.server.core.event.EventSubscriber;
import omega.gof.game.server.core.strategy.attack.AttackStrategy;
import omega.gof.game.server.factory.NonPlayingHumanFactory;
import omega.gof.game.server.model.human.NonPlayingHuman;
import omega.gof.game.server.model.human.properties.Intelligence;
import omega.gof.game.server.model.human.properties.Loyalty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class Environment implements Cloneable, Iterable<NonPlayingHuman> {

    private static final Logger LOGGER = LoggerFactory.getLogger(Environment.class);

    protected AttackStrategy attackStrategy;

    protected int numberOfSoldiersSword;
    protected int numberOfSoldiersGun;
    protected int numberOfSoldiersStrategy;
    protected int numberOfCivilianMaleFarmers;
    protected int numberOfCivilianFemaleFarmers;

    protected List<NonPlayingHuman> soldiersSword;
    protected List<NonPlayingHuman> soldiersGun;
    protected List<NonPlayingHuman> soldiersStrategy;
    protected List<NonPlayingHuman> civilianFemaleFarmers;
    protected List<NonPlayingHuman> civilianMaleFarmers;

    protected NonPlayingHumanFactory nonPlayingHumanFactory;

    public Environment() {
        this.nonPlayingHumanFactory = new NonPlayingHumanFactory();
        LOGGER.info("Initialize NPC Factory");
    }

    protected abstract void initializeSoldersSword();

    protected abstract void initializeSoldersGun();

    protected abstract void initializeSoldersStrategy();

    protected abstract void initializeCivilianFemaleFarmers();

    protected abstract void initializeCivilianMaleFarmers();

    public abstract void initializeEnvironment();

    public final void initialize() {
        initializeSoldersSword();
        initializeSoldersGun();
        initializeSoldersStrategy();
        initializeCivilianFemaleFarmers();
        initializeCivilianMaleFarmers();
        initializeEnvironment();
    }

    public AttackStrategy getAttackStrategy() {
        return attackStrategy;
    }

    public void setAttackStrategy(AttackStrategy attackStrategy) {
        this.attackStrategy = attackStrategy;
    }

    public void subscribeToGameEventPublisherOld(EventPublisher eventPublisher) {
        LOGGER.info("Subscribing to Game Event Publisher");

        if (soldiersSword != null) {
            for (NonPlayingHuman soldier : soldiersSword) {
                eventPublisher.subscribe((EventSubscriber) soldier);
            }
        }

        if (soldiersGun != null) {
            for (NonPlayingHuman soldier : soldiersGun) {
                eventPublisher.subscribe((EventSubscriber) soldier);
            }
        }

        if (soldiersStrategy != null) {
            for (NonPlayingHuman soldier : soldiersStrategy) {
                eventPublisher.subscribe((EventSubscriber) soldier);
            }
        }

        if (civilianFemaleFarmers != null) {
            for (NonPlayingHuman civilian : civilianFemaleFarmers) {
                eventPublisher.subscribe((EventSubscriber) civilian);
            }
        }

        if (civilianMaleFarmers != null) {
            for (NonPlayingHuman civilian : civilianMaleFarmers) {
                eventPublisher.subscribe((EventSubscriber) civilian);
            }
        }
    }

    public void subscribeToGameEventPublisher(EventPublisher eventPublisher) {
        LOGGER.info("Subscribing to Game Event Publisher");

        Iterator<NonPlayingHuman > itr = this.iterator();

        while(itr.hasNext()) {
            eventPublisher.subscribe((EventSubscriber)itr.next());
            LOGGER.info("Subscribed to EventPublisher");
        }
    }

    /**
     * This method is executed when a school system is build by the Game Player
     */
    public void increaseIntelligenceFromLowToMedium() {
        LOGGER.info("Subscribing to Game Event Publisher");

        Iterator<NonPlayingHuman > itr = this.iterator();

        while(itr.hasNext()) {
            itr.next().setIntelligence(Intelligence.MEDIUM);
            LOGGER.info("Subscribed to EventPublisher");
        }
    }


    /**
     * This method is executed when a university system is build by the Game Player
     */
    public void increaseIntelligenceFromMediumToHigh() {
        LOGGER.info("Subscribing to Game Event Publisher");

        Iterator<NonPlayingHuman > itr = this.iterator();

        while(itr.hasNext()) {
            itr.next().setIntelligence(Intelligence.HIGH);
            LOGGER.info("Subscribed to EventPublisher");
        }
    }


    /**
     * This method is periodically invoked to revise the loyalty of the NonPlayingHumans
     */
    public void revisePopulationLoyalty() {
        LOGGER.info("Revising population loyalty");

        Iterator<NonPlayingHuman > itr = this.iterator();

        AIFacade aiFacade = new AIFacade();

        while(itr.hasNext()) {
            NonPlayingHuman nonPlayingHuman = itr.next();
            Loyalty newLevel = (Loyalty)aiFacade.getNewLoyaltyLevel(nonPlayingHuman.getLoyalty(), nonPlayingHuman.getType());
            nonPlayingHuman.setLoyalty(newLevel);
        }
    }


    public void revisePopulationStamina() {

    }




    @Override
    public Iterator<NonPlayingHuman> iterator() {
        return new EnvironmentIterator();
    }

    class EnvironmentIterator implements Iterator<NonPlayingHuman> {

        List<NonPlayingHuman> tempList;
        int itemCounter = 0;

        public EnvironmentIterator() {
            tempList = new ArrayList<>();
            if (soldiersSword != null)
                tempList.addAll(soldiersSword);

            if (soldiersGun != null)
                tempList.addAll(soldiersGun);

            if (soldiersStrategy != null)
                tempList.addAll(soldiersStrategy);

            if (civilianFemaleFarmers != null)
                tempList.addAll(civilianFemaleFarmers);

            if (civilianMaleFarmers != null)
                tempList.addAll(civilianMaleFarmers);
        }

        @Override
        public boolean hasNext() {
            return itemCounter < tempList.size() && this.tempList.get(itemCounter) != null;
        }

        @Override
        public NonPlayingHuman next() {
            return this.tempList.get(itemCounter++);
        }
    }




    @Override
    protected java.lang.Object clone(){
        Object object = null;
        try {
            object = super.clone();
        } catch (Exception e) {
            LOGGER.error("Error in creating clone");
        }

        return object;
    }
}
