package omega.gof.game.server.model.human.powerup;

import omega.gof.game.server.model.human.NonPlayingHuman;

public class BasicPowerUp implements PowerUp {

    final int POWER_UP_ADVANTAGE = 1;

    @Override
    public int getPowerUpFactor() {
        return POWER_UP_ADVANTAGE;
    }
}
