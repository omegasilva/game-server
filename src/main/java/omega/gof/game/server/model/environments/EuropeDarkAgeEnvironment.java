package omega.gof.game.server.model.environments;

import omega.gof.game.server.factory.HumanType;
import omega.gof.game.server.model.human.NonPlayingHuman;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class EuropeDarkAgeEnvironment extends Environment {

    private static final Logger LOGGER = LoggerFactory.getLogger(EuropeDarkAgeEnvironment.class);

    protected int numberOfSoldiersSword = 10;
    protected int numberOfSoldiersGun = 0;
    protected int numberOfSoldiersStrategy = 10;
    protected int numberOfCivilianMaleFarmers = 10;
    protected int numberOfCivilianFemaleFarmers = 10;

    @Override
    protected void initializeSoldersSword() {
        LOGGER.info("Initialize Soldiers Sword in Europe Dark Age Environment");
        this.soldiersSword = new ArrayList<>();

        for (int i = 0; i < numberOfSoldiersSword; i++) {
            this.soldiersSword.add((NonPlayingHuman)this.nonPlayingHumanFactory
                    .createNonPlayingHuman(HumanType.SOLDIER_SWORD, this));
        }
    }

    @Override
    protected void initializeSoldersGun() {

    }

    @Override
    protected void initializeSoldersStrategy() {

    }

    @Override
    protected void initializeCivilianFemaleFarmers() {

    }

    @Override
    protected void initializeCivilianMaleFarmers() {

    }

    @Override
    public void initializeEnvironment() {

    }
}
