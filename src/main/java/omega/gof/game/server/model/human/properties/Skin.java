package omega.gof.game.server.model.human.properties;

public enum Skin {
    DEFAULT,
    VILLAGER,
    SOLDIER,
    SPY,
    MODERN
}
