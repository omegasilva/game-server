package omega.gof.game.server.model.human.properties;

public enum Type {
    CIVILIAN_MALE,
    CIVILIAN_FEMALE,
    SOLDIER,
    PROPHET
}
