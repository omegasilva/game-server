package omega.gof.game.server.model.environments;

import omega.gof.game.server.core.strategy.attack.DivideAndConquer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class EnvironmentRegistry {

    private static final Logger LOGGER = LoggerFactory.getLogger(EnvironmentRegistry.class);

    private static Map<EnvironmentType, Environment> environmentRegistry;

    public static void initialize() {
        LOGGER.info("Initialize Environment Registry");
        environmentRegistry = new HashMap<>();

        Environment stoneAgeEnvironment = new StoneAgeEnvironment();
        stoneAgeEnvironment.setAttackStrategy(new DivideAndConquer());
        stoneAgeEnvironment.initialize();
        environmentRegistry.put(EnvironmentType.STONE_AGE, stoneAgeEnvironment);

        Environment europeDarkAgeEnvironment = new EuropeDarkAgeEnvironment();
        europeDarkAgeEnvironment.setAttackStrategy(new DivideAndConquer());
        europeDarkAgeEnvironment.initialize();
        environmentRegistry.put(EnvironmentType.EUROPE_DARK_AGE, europeDarkAgeEnvironment);

        Environment firstWorldWarEnvironment = new FirstWorldWarEuropeEnvironment();
        firstWorldWarEnvironment.setAttackStrategy(new DivideAndConquer());
        firstWorldWarEnvironment.initialize();
        environmentRegistry.put(EnvironmentType.FIRST_WORLD_WAR_EUROPE, firstWorldWarEnvironment);
    }

    public static Environment getEnvironment(EnvironmentType environmentType) {
        LOGGER.info("Cloning environment type " + environmentType.name());
        Environment environment = environmentRegistry.get(environmentType);
        return (Environment) environment.clone();
    }
}
