package omega.gof.game.server.model.human.powerup;

import omega.gof.game.server.model.human.NonPlayingHuman;

public class StealthPowerUp extends ExperiencePowerUp {


    final int POWER_UP_ADVANTAGE = 5;

    public StealthPowerUp(PowerUp powerUp) {
        super(powerUp);
    }

    @Override
    public int getPowerUpFactor() {
        return super.getPowerUpFactor() + POWER_UP_ADVANTAGE;
    }
}
