package omega.gof.game.server.factory;

import omega.gof.game.server.model.environments.Environment;
import omega.gof.game.server.model.human.Human;
import omega.gof.game.server.model.human.NonPlayingHuman;
import omega.gof.game.server.model.human.properties.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NonPlayingHumanFactory {

    private static final Logger LOGGER = LoggerFactory.getLogger(NonPlayingHumanFactory.class);

    public Human createNonPlayingHuman(HumanType humanType, Environment environment) {
        Human human = null;

        if (humanType == HumanType.SOLDIER_GUN) {
            human = new NonPlayingHuman(Armor.MEDIUM, Intelligence.MEDIUM, Loyalty.HIGH,
                    MovementPattern.FUNCTION, Skill.BATTLE_GUN, Skin.SOLDIER, Stamina.HIGH,
                    Type.SOLDIER, environment);
            LOGGER.info("Created NPC : " + HumanType.SOLDIER_GUN);
        } else if (humanType == HumanType.SOLIDER_STRATEGY) {
            human = new NonPlayingHuman.NonPlayingHumanBuilder(environment)
                    .withArmor(Armor.MEDIUM)
                    .withType(Type.SOLDIER)
                    .withSkill(Skill.BATTLE_STRATEGY)
                    .build();
            LOGGER.info("Created NPC : " + HumanType.SOLIDER_STRATEGY);
        } else if (humanType == HumanType.SOLDIER_SWORD) {
            human = new NonPlayingHuman.NonPlayingHumanBuilder(environment)
                    .withArmor(Armor.MEDIUM)
                    .withType(Type.SOLDIER)
                    .withSkill(Skill.BATTLE_SWORD)
                    .build();
            LOGGER.info("Created NPC : " + HumanType.SOLDIER_SWORD);
        }

        return human;
    }

}
