package omega.gof.game.server.factory;

public enum HumanType {
    SOLDIER_SWORD,
    SOLDIER_GUN,
    SOLIDER_STRATEGY,
    CIVILIAN_MALE_FARMER,
    CIVILIAN_FEMALE_FARMER,
    CIVILIAN_MALE_WOODWORKER
}
