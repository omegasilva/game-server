package omega.gof.game.server.ai.factory;

import omega.gof.game.server.ai.strategy.conversion.BinaryLevelConversionStrategy;
import omega.gof.game.server.ai.strategy.conversion.DefaultLevelConversionStrategy;
import omega.gof.game.server.ai.strategy.conversion.NonLinearLevelConversionStrategy;

public class ConversionStrategyFactory {

    public static omega.gof.game.server.ai.strategy.conversion.LevelConversionStrategy getConversionStrategy(LevelConversionStrategyType levelConversionStrategyType) {
        switch (levelConversionStrategyType) {
            case BINARY:
                return new BinaryLevelConversionStrategy();
            case NON_LINEAR:
                return new NonLinearLevelConversionStrategy();
            default:
                return new DefaultLevelConversionStrategy();
        }
    }
}
