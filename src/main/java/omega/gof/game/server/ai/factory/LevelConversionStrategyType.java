package omega.gof.game.server.ai.factory;

public enum LevelConversionStrategyType {
    NON_LINEAR,
    BINARY
}
