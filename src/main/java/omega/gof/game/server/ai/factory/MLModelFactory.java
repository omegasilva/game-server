package omega.gof.game.server.ai.factory;

import omega.gof.game.server.ai.*;
import omega.gof.game.server.ai.strategy.conversion.LevelConversionStrategy;

public class MLModelFactory {

    public static MLModel getMLModel(MLModelType mlModelType) {

        MLModel mlModel = null;

        if (mlModelType == MLModelType.LOYALTY) {
            mlModel = new LoyaltyRegressionModelAdapter(new LoyaltyRegressionModel(), LevelConversionStrategyType.BINARY);
        } else if (mlModelType == MLModelType.STAMINA) {
            mlModel = new StaminaRegressionModelAdapter(new StaminaRegressionModel(), null);
        }

        return mlModel;
    }
}
