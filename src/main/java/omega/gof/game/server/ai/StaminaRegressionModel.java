package omega.gof.game.server.ai;

import omega.gof.game.server.model.human.properties.Type;

import java.util.Random;

public class StaminaRegressionModel implements RegressionModel {
    @Override
    public int getForecastedValue(int currentValue, Type type) {
        int upperBound = 30;
        Random random = new Random();
        return random.nextInt(upperBound);
    }
}
