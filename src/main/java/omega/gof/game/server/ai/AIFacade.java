package omega.gof.game.server.ai;

import omega.gof.game.server.ai.factory.MLModelFactory;
import omega.gof.game.server.ai.factory.MLModelType;
import omega.gof.game.server.model.human.properties.Type;

public class AIFacade {

    public Enum getNewLoyaltyLevel(Enum<?> currentLevel, Type type) {
        MLModel adapter = MLModelFactory.getMLModel(MLModelType.LOYALTY);
        return adapter.getNewLevel(currentLevel, type);
    }

    public Enum getNewStaminaLevel(Enum<?> currentLevel, Type type) {
        MLModel adapter = MLModelFactory.getMLModel(MLModelType.STAMINA);
        return adapter.getNewLevel(currentLevel, type);
    }

    public Enum getNewIntelligenceLevel(Enum<?> currentLevel, Type type) {
        return null;
    }
}
