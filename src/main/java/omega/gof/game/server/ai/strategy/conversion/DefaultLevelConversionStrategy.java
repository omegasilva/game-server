package omega.gof.game.server.ai.strategy.conversion;

public class DefaultLevelConversionStrategy implements LevelConversionStrategy {

    @Override
    public int convert(String levelName) {
        int level = 0;

        if (levelName.equalsIgnoreCase("high")) {
            level = 30;
        } else if (levelName.equalsIgnoreCase("medium")) {
            level = 20;
        } else {
            level = 10;
        }

        return level;
    }
}
