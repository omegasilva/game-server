package omega.gof.game.server.ai.strategy.conversion;

public interface LevelConversionStrategy {

    int convert(String levelName);
}
