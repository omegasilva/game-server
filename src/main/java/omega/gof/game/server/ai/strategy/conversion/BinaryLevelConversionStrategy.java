package omega.gof.game.server.ai.strategy.conversion;

public class BinaryLevelConversionStrategy implements LevelConversionStrategy{

    @Override
    public int convert(String levelName) {
        int level = 0;

        if (levelName.equalsIgnoreCase("high")) {
            level = 1;
        }

        return level;
    }
}
