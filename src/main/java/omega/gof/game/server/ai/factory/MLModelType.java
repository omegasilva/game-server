package omega.gof.game.server.ai.factory;

public enum MLModelType {
    LOYALTY,
    STAMINA,
    INTELLIGENCE
}
