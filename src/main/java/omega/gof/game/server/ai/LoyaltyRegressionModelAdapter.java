package omega.gof.game.server.ai;

import omega.gof.game.server.ai.factory.ConversionStrategyFactory;
import omega.gof.game.server.ai.factory.LevelConversionStrategyType;
import omega.gof.game.server.ai.strategy.conversion.LevelConversionStrategy;
import omega.gof.game.server.model.human.properties.Loyalty;
import omega.gof.game.server.model.human.properties.Type;

public class LoyaltyRegressionModelAdapter implements MLModel {

    private RegressionModel regressionModel;
    private LevelConversionStrategy levelConversionStrategy;

    public LoyaltyRegressionModelAdapter(RegressionModel regressionModelParam, LevelConversionStrategyType levelConversionStrategyType) {
        this.regressionModel = regressionModelParam;
        this.levelConversionStrategy = ConversionStrategyFactory.getConversionStrategy(levelConversionStrategyType);
    }

    @Override
    public Enum getNewLevel(Enum<?> currentLevel, Type type) {
        // Convert level to an integer
        int level = levelConversionStrategy.convert(currentLevel.name());

        int forecastedValue = regressionModel.getForecastedValue(level, type);

        // convert the forecasted value to a level
        Loyalty forecastedLevel;
        if (forecastedValue > 20) {
            forecastedLevel = Loyalty.HIGH;
        } else if (forecastedValue > 10) {
            forecastedLevel = Loyalty.MEDIUM;
        } else {
            forecastedLevel = Loyalty.LOW;
        }

        return forecastedLevel;
    }
}
