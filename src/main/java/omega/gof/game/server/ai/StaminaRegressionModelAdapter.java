package omega.gof.game.server.ai;

import omega.gof.game.server.ai.factory.ConversionStrategyFactory;
import omega.gof.game.server.ai.factory.LevelConversionStrategyType;
import omega.gof.game.server.ai.strategy.conversion.DefaultLevelConversionStrategy;
import omega.gof.game.server.ai.strategy.conversion.LevelConversionStrategy;
import omega.gof.game.server.model.human.properties.Loyalty;
import omega.gof.game.server.model.human.properties.Stamina;
import omega.gof.game.server.model.human.properties.Type;

public class StaminaRegressionModelAdapter implements MLModel {

    private RegressionModel regressionModel;
    private LevelConversionStrategy levelConversionStrategy;

    public StaminaRegressionModelAdapter(RegressionModel regressionModelParam,
                                         LevelConversionStrategyType levelConversionStrategyType) {
        this.regressionModel = regressionModelParam;
        this.levelConversionStrategy = ConversionStrategyFactory.getConversionStrategy(levelConversionStrategyType);
    }

    @Override
    public Enum<?> getNewLevel(Enum<?> currentLevel, Type type) {
        // Convert level to an integer

        int level = levelConversionStrategy.convert(currentLevel.name());

        int forecastedValue = regressionModel.getForecastedValue(level, type);

        // convert the forecasted value to a level
        Stamina forecastedLevel;
        if (forecastedValue > 20) {
            forecastedLevel = Stamina.HIGH;
        } else if (forecastedValue > 10) {
            forecastedLevel = Stamina.MEDIUM;
        } else {
            forecastedLevel = Stamina.LOW;
        }

        return forecastedLevel;
    }
}
