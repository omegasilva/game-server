package omega.gof.game.server.ai;

import omega.gof.game.server.model.human.properties.Type;

public interface RegressionModel {

    int getForecastedValue(int currentValue, Type type);
}
