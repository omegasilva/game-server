package omega.gof.game.server.ai.strategy.conversion;

public class NonLinearLevelConversionStrategy implements LevelConversionStrategy {

    @Override
    public int convert(String levelName) {
        int level = 0;

        if (levelName.equalsIgnoreCase("high")) {
            level = 30;
        } else if (levelName.equalsIgnoreCase("medium")) {
            level = 10;
        } else {
            level = 1;
        }

        return level;
    }
}
